
/**
 * Main application class.
 *
 * @author m_gichevky
 */
public class Main {

    private static final int fibonacchiAlgoritm = 1;
    private static final int factorialAlgoritm = 2;
    private static final int loopWhile = 1;
    private static final int loopDoWhile = 2;
    private static final int loopFor = 3;

    /**
     * Application start point.
     * Take values from scanner and analyzes them
     */
    public static void main(String[] args) {
        final int algoritmId = parseInt(args[0]);
        final int loopType = parseInt(args[1]);
        final int n = parseInt(args[2]);
        checkAndProcessValues(algoritmId, loopType, n);
    }

    /**
     * Method for parse integer argument
     *
     * @param arg actual argument
     * @return parsed integer argument
     */
    private static int parseInt(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect argument " + arg, e);
        }
    }

    /**
     * Check values for incorrect arguments and chooses type of algoritm
     *
     * @param algoritmId - type of algoritm
     * @param loopType - selected loop type
     * @param n - number of iterations
     */
    private static void checkAndProcessValues(int algoritmId, int loopType, int n) {
        if ((algoritmId > 2) || (algoritmId <=0) || (loopType > 3) || (loopType <=0) || (n < 0)) {
            throw new IllegalArgumentException("Incorrect arguments");
        } else {
            if (algoritmId == fibonacchiAlgoritm) {
                int[] fib = fibonacchi(loopType, n);
                for (int i = 0; i < n; i++) {
                    System.out.print(fib[i] + " ");
                }
            }
            if (algoritmId == factorialAlgoritm) {
                int result = factorial(loopType, n);
                System.out.println(result);
            }
        }
    }

    /**
     * Calculation of a series of Fibonacci numbers
     * Method chooses loopType and works on selected loop
     *
     * @param loopType - selected loop type
     * @param n - number of iterations
     * @return f - array of Fibonacchi numbers
     */
    private static int[] fibonacchi(int loopType, int n) {
        int[] fib = new int[n+1];
        int i = 0;
        int previousValue = 0;
        int currentValue = 1;
        int temporaryValue;
        if (loopType == loopWhile) {
            while (i < n) {
                fib[i] = previousValue;
                temporaryValue = currentValue;
                currentValue = previousValue + currentValue;
                previousValue = temporaryValue;
                i++;
            }
        }
        if (loopType == loopDoWhile) {
            do {
                fib[i] = previousValue;
                temporaryValue = currentValue;
                currentValue = previousValue + currentValue;
                previousValue = temporaryValue;
                i++;
            } while (i < n);
        }
        if (loopType == loopFor) {
            for (i = 0; i < n; i++) {
                fib[i] = previousValue;
                temporaryValue = currentValue;
                currentValue = previousValue + currentValue;
                previousValue = temporaryValue;
            }
        }
        return fib;
    }

    /**
     * Factorial calculation
     * Method chooses loopType and works on selected loop
     *
     * @param loopType - selected loop type
     * @param n - number of iterations
     * @return result - factorial result
     */
    public static int factorial(int loopType, int n) {
        int i = 0;
        int result = 0;
        if (loopType == loopWhile) {
            while (i <= n) {
                result = result * i;
                if (result == 0) result = 1;
                i++;
            }
        }
        if (loopType == loopDoWhile) {
            do {
                result = result * i;
                if (result == 0) result = 1;
                i++;
            } while (i <= n);
        }
        if (loopType == loopFor) {
            for (i = 0; i <= n; i++) {
                result = result * i;
                if (result == 0) result = 1;
            }
        }
        return result;
    }
}
