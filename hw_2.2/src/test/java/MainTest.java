import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;

/**
 * MainTest.
 *
 * @author m_gichevsky
 */
public class MainTest {
    @Test
    public void testFactorialWhenPositiveNumber() {
        int result = Main.factorial(1, 5);
        Assert.assertEquals(120, result);
        result = Main.factorial(2, 6);
        Assert.assertEquals(720, result);
        result = Main.factorial(3, 7);
        Assert.assertEquals(5040, result);
    }

    @Test
    public void testForPositiveInputFactorial() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        String[] arguments = {"2", "1", "5"};
        Main.main(arguments);
        String result = new String(output.toByteArray()).trim();
        Assert.assertEquals("120",result);
    }

    @Test
    public void testForPositiveInputFibonacci() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        String[] arguments = {"1", "1", "5"};
        Main.main(arguments);
        String result = new String(output.toByteArray()).trim();
        Assert.assertEquals("0 1 1 2 3",result);
    }

    @Test
    public void testForInputZeroNumberFactorial() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        String[] arguments = {"2", "2", "0"};
        Main.main(arguments);
        String result = new String(output.toByteArray()).trim();
        Assert.assertEquals("1",result);
    }

    @Test
    public void testForInputZeroNumberFibonacchi() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        String[] arguments = {"1","3","0"};
        Main.main(arguments);
        String result = new String(output.toByteArray()).trim();
        Assert.assertEquals("",result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testForNegativeLoopType() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        String[] arguments = {"1", "-1", "5"};
        Main.main(arguments);
        String result = new String(output.toByteArray()).trim();
        Assert.assertEquals("",result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testForIncorrectValues() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        String[] arguments = {"adv", "0", "123"};
        Main.main(arguments);
        String result = new String(output.toByteArray()).trim();
        Assert.assertEquals("",result);
    }

}
