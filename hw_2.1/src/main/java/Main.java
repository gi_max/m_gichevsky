import static java.lang.Math.*;

/**
 * Main application class.
 *
 * @author m_gichevky
 */
public class Main {

    /**
     * Application start point.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        final int a = parseInt(args[0]);
        final int p = parseInt(args[1]);
        final double m1 = parseDouble(args[2]);
        final double m2 = parseDouble(args[3]);
        System.out.println("G = " + calculate(a, p, m1, m2));
    }

    /**
     * Method for calculating by the formula
     *
     * @param a - first argument a
     * @param p - second argument p
     * @param m1 - third argument m1
     * @param m2 - fourth argument m2
     * @return Answer calculated by the formula
     */
    public static double calculate(int a, int p, double m1, double m2) {
        if (p == 0 || (m1 + m2 == 0)) {
            throw new ArithmeticException("Division by zero");
        }
        return 4 * pow(Math.PI, 2) * (pow(a, 3) / (pow(p, 2) * (m1 + m2)));
    }

    /**
     * Method for parse integer argument
     *
     * @param arg actual argument
     * @return parsed integer argument
     */
    public static int parseInt(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect argument " + arg, e);
        }
    }

    /**
     * Method for parse double argument
     *
     * @param arg actual argument
     * @return parsed double argument
     */
    public static double parseDouble(String arg) {
        try {
            return Double.parseDouble(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect argument " + arg, e);
        }
    }
}
