import org.junit.Assert;
import org.junit.Test;

/**
 * MainTest.
 *
 * @author m_gichevsky
 */
public class MainTest {

    @Test
    public void testForPositiveResults() {
        double result = Main.calculate(3, 2, 3.5, 5.6);
        Assert.assertEquals(29.28344, result,0.00001);
        result = Main.calculate(4, 2, 6.7, 9.8);
        Assert.assertEquals(38.28210, result, 0.00001);
        result = Main.calculate(2, 3, 7.8, 6.9);
        Assert.assertEquals(2.38721, result, 0.00001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseWithNullArgument() {
        int result = Main.parseInt(null);
        Assert.assertEquals(2, result);
    }

    @Test(expected = ArithmeticException.class)
    public void testForDivisionByZero() {
        int result = (int) Main.calculate(3, 0, 5.5, 3.2);
        Assert.assertEquals(2, result);
    }
}
