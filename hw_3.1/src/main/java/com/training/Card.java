package com.training;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Class Card for work with balance
 * @author m_gichevsky
 */
public class Card {
    /** Field ownerName */
    private String ownerName;
    /** Field balance */
    private BigDecimal balance = new BigDecimal(0);

    /**
     * Constructor - creating new object with parametrs
     * @param ownerName - name of card owner
     * @param value - balance value, input only in BigDecimal
     */
    Card(String ownerName, BigDecimal value) {
        this.ownerName = ownerName;
        this.balance = value;
        this.balance = balance.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Constructor - creating new object with 1 parametr ownerName
     * @param ownerName - name of card owner
     */
    Card(String ownerName) {
        this.ownerName = ownerName;
    }

    /**
     * This getter gets ownerName value
     * @return ownerName
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     * This getter gets balance value
     * @return balance in BigDecimal
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Add to current balance new value
     * @param addValue value to be added to balance in BigDecimal
     */
    public void addToBalance(BigDecimal addValue) {
        this.balance = balance.add(addValue).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Substract value from balance
     * @param withdrawValue value to be substracted from balance in BigDecimal
     */
    public void withdrawFromBalance(BigDecimal withdrawValue) {
        this.balance = balance.subtract(withdrawValue).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Additional method to display Card balance in different currency (dollars) by
     * providing exchange rate.
     * @param courceOfCurrency - current cource of dollar
     * @return value of balance in dollars (BigDecimal)
     */
    public BigDecimal displayInDollars(BigDecimal courceOfCurrency) {
        return this.balance.divide(courceOfCurrency, 2, RoundingMode.HALF_DOWN);
    }
}
