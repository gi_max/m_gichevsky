package com.training;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 *  CardTest
 *
 * @author m_gichevsky
 */
public class CardTest {

    @Test
    public void testAddToBalance() {
        BigDecimal addValue = new BigDecimal("625848.44");
        Card card1 = new Card("First");
        card1.addToBalance(addValue);
        Assert.assertEquals(addValue, card1.getBalance());
    }

    @Test
    public void testWithdrawFromBalance() {
        BigDecimal testValue = new BigDecimal("852156.56");
        BigDecimal withdraw = new BigDecimal("598123.32");
        BigDecimal expected = new BigDecimal("254033.24");
        Card card1 = new Card("First", testValue);
        card1.withdrawFromBalance(withdraw);
        Assert.assertEquals(expected, card1.getBalance());
    }

    @Test
    public void testAddNegativeValue() {
        BigDecimal testValue = new BigDecimal("255356.34");
        BigDecimal addValue = new BigDecimal("-345543.32");
        BigDecimal expected = new BigDecimal("-90186.98");
        Card card1 = new Card("First", testValue);
        card1.addToBalance(addValue);
        Assert.assertEquals(expected, card1.getBalance());
    }

    @Test
    public void testDisplayInDollars() {
        BigDecimal testValue = new BigDecimal(500);
        BigDecimal cource = new BigDecimal(2.0544);
        BigDecimal expected = new BigDecimal(243.38).setScale(2, BigDecimal.ROUND_HALF_UP);
        Card card1 = new Card("First", testValue);
        Assert.assertEquals(expected, card1.displayInDollars(cource));
    }

    @Test(expected = ArithmeticException.class)
    public void testDivisionByZero() {
        BigDecimal testValue = new BigDecimal(322);
        BigDecimal cource = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal expected = new BigDecimal(243.38).setScale(2, BigDecimal.ROUND_HALF_UP);
        Card card1 = new Card("First", testValue);
        Assert.assertEquals(expected, card1.displayInDollars(cource));
    }
}
