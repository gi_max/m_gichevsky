package com.training.exceptions;

/**
 * Class ValidationFailedException build
 * validation exception if there are discrepancies.
 */
public class ValidationFailedException extends Exception {

    /**
     * Constructor of ValidationFailedException
     * which call constructor from Exception.
     * @param errorMessage - message which will be displayed in the console
     */
    public ValidationFailedException(final String errorMessage) {
        super(errorMessage);
    }
}
