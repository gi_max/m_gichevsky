package com.training;

import com.training.exceptions.ValidationFailedException;

/**
 * Interface Validator for call methods of other type validators.
 * @param <T> - generic which will take class
 */
public interface Validator<T> {
    /**
     * Method validateWithType which will called in other classes.
     * @param validateValue - value to be validated
     * @throws ValidationFailedException - this exception is thrown
     * if the condition is not met
     */
    void validateWithType(T validateValue) throws ValidationFailedException;
}
