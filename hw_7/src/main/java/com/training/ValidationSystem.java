package com.training;

import com.training.exceptions.ValidationFailedException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class ValidationSystem is the utility class
 * for validate values of different types.
 */
public class ValidationSystem {

    /**
     * Initialization Map for choosing types.
     */
    private static Map<String, Validator> availableClasses =
            new HashMap<String, Validator>();

    /**
     * This method determines the type of incoming data
     * and calls the corresponding validation.
     * @param object - value you need to validate
     * @param <T> - class type
     * @throws ValidationFailedException - this exception is thrown
     * if the condition is not met
     */
    public static <T> void validate(final T object)
            throws ValidationFailedException {
        availableClasses.put(Integer.class.getName(), new IntegerValidator());
        availableClasses.put(String.class.getName(), new StringValidator());

        Validator<T> validator =
                availableClasses.get(object.getClass().getCanonicalName());

        try {
            validator.validateWithType(object);
        } catch (NullPointerException exception) {
            throw new ValidationFailedException("Wrong values");
        }
    }
}
