package com.training;

import com.training.exceptions.ValidationFailedException;

/**
 * Class IntegerValidator uses for validation Integer classes.
 */
public class IntegerValidator implements Validator<Integer> {
    /**
     * Determining of the minimum integer value.
     */
    private static final int MIN_VALUE = 1;
    /**
     * Determining of the maximum integer value.
     */
    private static final int MAX_VALUE = 10;

    /**
     * Method validateWithType which will called in other classes.
     *
     * @param validateValue - value to be validated
     * @throws ValidationFailedException - this exception is thrown
     * if the condition is not met
     */
    public final void validateWithType(final Integer validateValue)
            throws ValidationFailedException {

        if ((validateValue < MIN_VALUE) || (validateValue > MAX_VALUE)) {
            throw new ValidationFailedException("Integer value are incorrect");
        }
    }
}
