package com.training;

import com.training.exceptions.ValidationFailedException;

/**
 * Class StringValidator uses for validation String classes.
 */
public class StringValidator implements Validator<String> {

    /**
     * Method validateWithType which will called in other classes.
     *
     * @param validateValue - value to be validated
     * @throws ValidationFailedException - this exception is thrown
     * if the condition is not met
     */
    public final void validateWithType(final String validateValue)
            throws ValidationFailedException {
        if (validateValue.equals("")) {
            throw new ValidationFailedException("Your String is empty");
        }
        char firstLetter = validateValue.charAt(0);
        if (!((firstLetter >= 'A') && (firstLetter <= 'Z'))) {
            throw new ValidationFailedException("String value are incorrect");
        }
    }
}
