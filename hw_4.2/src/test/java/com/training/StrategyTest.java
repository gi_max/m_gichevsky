package com.training;

import org.junit.Assert;
import org.junit.Test;

/**
 * StrategyTest
 *
 * @author m_gichevsky
 */
public class StrategyTest {
    @Test
    public void testConcreteStrategyBubbleSort() {
        Strategy sortStrategy = new ConcreteStrategyBubbleSort();
        SortingContext context = new SortingContext(sortStrategy);
        int[] array = context.execute(new int[]{1, 25, 4, 79, 2, 54, 32, 5});
        Assert.assertArrayEquals(new int[] {1, 2, 4, 5, 25, 32, 54, 79}, array);
    }

    @Test
    public void testConcreteStrategySelectionSort() {
        Strategy sortStrategy = new ConcreteStrategySelectionSort();
        SortingContext context = new SortingContext(sortStrategy);
        int[] array = context.execute(new int[] {64, 42, 56, 1, 66, 43, 79, 80, 93, 5, 6, 3});
        Assert.assertArrayEquals(new int[] {1, 3, 5, 6, 42, 43, 56, 64, 66, 79, 80, 93}, array);
    }

    @Test
    public void testConcreteStrategyBubbleSortAllValuesAreZero() {
        Strategy sortStrategy = new ConcreteStrategyBubbleSort();
        SortingContext context = new SortingContext(sortStrategy);
        int[] array = context.execute(new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
        Assert.assertArrayEquals(new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, array);
    }

    @Test
    public void testConcreteStrategySelectionSortWithNegativeValues() {
        Strategy sortStrategy = new ConcreteStrategySelectionSort();
        SortingContext context = new SortingContext(sortStrategy);
        int[] array = context.execute(new int[] {-5, 10, 3, -43, 0, 32, -6, 1});
        Assert.assertArrayEquals(new int[] {-43, -6, -5, 0, 1, 3, 10, 32}, array);
    }
}
