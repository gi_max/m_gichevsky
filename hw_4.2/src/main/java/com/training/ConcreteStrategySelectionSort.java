package com.training;

/**
 * Class for sorting by Selection sort method.
 */
public class ConcreteStrategySelectionSort implements Strategy {

    /**
     * Selection sort method.
     * @param array - array of integer
     * @return - sorted array
     */
    public int[] execute(final int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minId = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minId = j;
                }
            }
            int temp = array[i];
            array[i] = min;
            array[minId] = temp;
        }
        return array;
    }
}
