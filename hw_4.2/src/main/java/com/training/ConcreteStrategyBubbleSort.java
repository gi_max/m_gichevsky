package com.training;

/**
 * Class for sorting by Bubble sort method.
 */
public class ConcreteStrategyBubbleSort implements Strategy {

    /**
     * Bubble sort method.
     * @param array - array of integer
     * @return - sorted array
     */
    public int[] execute(final int[] array) {
        boolean sorted = false;
        int temp;
        while(!sorted) {
            sorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    sorted = false;
                }
            }
        }
        return array;
    }
}
