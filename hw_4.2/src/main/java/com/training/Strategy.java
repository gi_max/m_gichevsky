package com.training;

/**
 * Interface Strategy for call concrete strategy.
 */
public interface Strategy {
    /**
     * Will take arrays for selected strategy.
     * @param array - array of integer
     * @return - sorted array
     */
    int[] execute(int[] array);
}
