package com.training;

/**
 *  SortingContext class using the strategy interface.
 */
public class SortingContext {
    private Strategy strategy;

    /**
     * Constructor which take and set selected strategy.
     * @param sortStrategy - selected sort strategy
     */
    public SortingContext(final Strategy sortStrategy) {
        this.strategy = sortStrategy;
    }

    /**
     * Method which take and returned execute from selected sorting.
     * @param array - array of integer
     * @return - array value from selected sorting
     */
    public int[] execute(final int[] array) {
        return strategy.execute(array);
    }
}
