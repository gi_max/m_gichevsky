package com.training;

public class Median {

    /**
     * This method sorts float array
     * @param arr - current int array
     */
    private static void sorting(int[] arr) {
        int tempValue;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    tempValue = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tempValue;
                }
            }
        }
    }

    /**
     * This method sorts double array
     * @param arr - current double array
     */
    private static void sorting(double[] arr) {
        double tempValue;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    tempValue = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tempValue;
                }
            }
        }
    }

    /**
     * This method takes array in int data type and finds a median
     * @param arr - current int array
     * @return - median
     */
    public static float median(int[] arr) {
        sorting(arr);
        if (arr.length % 2 == 1) {
            return arr[arr.length/2];
        } else {
            return (arr[arr.length / 2] + arr[arr.length / 2 - 1]) / (float) 2;
        }
    }

    /**
     * This method takes array in double data type and finds a median
     * @param arr - current double array
     * @return - median
     */
    public static double median(double[] arr) {
        sorting(arr);
        if (arr.length % 2 == 1) {
            return arr[arr.length/2];
        } else {
            return (arr[arr.length / 2] + arr[arr.length / 2 - 1]) / (double) 2;
        }
    }
}
