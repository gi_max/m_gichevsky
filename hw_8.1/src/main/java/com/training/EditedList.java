package com.training;

import com.training.exceptions.AttemptAddToFullListException;
import java.util.ArrayList;

/**
 * Class EditedList<T> for realization of List<T> interface.
 * @param <T> - selected class
 */
public class EditedList<T> extends ArrayList<T> {

    /**
     * Default size of list.
     */
    private static final int DEFAULT_MAXSIZE = 10;

    /**
     * Create empty object with default maxsize.
     */
    private static final Object[] EMPTY_ELEMENTS = new Object[DEFAULT_MAXSIZE];

    /**
     * Size of our list.
     */
    private static int size = 0;

    /**
     * maxSize - border for add method.
     */
    private static int maxSize = DEFAULT_MAXSIZE;

    /**
     * Array for our list with selected class.
     */
    private T[] elements;

    /**
     * Constructor with possibility to set maxSize.
     * @param initialCapacity - new maxSize
     */
    public EditedList(final int initialCapacity) {
        if (initialCapacity >= 0) {
            maxSize = initialCapacity;
            Object[] initElements = new Object[initialCapacity];
            this.elements = (T[]) initElements;
        } else {
            throw new IllegalArgumentException("Illegal size");
        }
    }

    /**
     * Default constructor.
     */
    public EditedList() {
        this.elements = (T[]) EMPTY_ELEMENTS;
    }

    /**
     * Return size of list.
     * @return - size of list
     */
    @Override
    public final int size() {
        return size;
    }

    /**
     * Checks if the list is empty.
     * @return true - is empty
     */
    @Override
    public final boolean isEmpty() {
        return size == 0;
    }

    /**
     * Get object by index from list.
     * @param index - index of list object
     * @return - object by index
     */
    @Override
    public final T get(final int index) {
        if (checkIndex(index)) {
            return elements[index];
        }
        return null;
    }

    /**
     * Set new object to list in index position
     * and return old value from index position.
     * @param index - position in list
     * @param element - object you need to set
     * @return - old value from list
     */
    @Override
    public final T set(final int index, final T element) {
        if (checkIndex(index)) {
            T oldValue = elements[index];
            elements[index] = element;
            return oldValue;
        }
        return null;
    }

    /**
     * Added value to list.
     * @param t - value you need to add
     * @return true if value successfully added
     */
    @Override
    public final boolean add(final T t) {
        if ((size + 1) <= maxSize) {
            this.elements[size] = t;
            size++;
            return true;
        } else {
            throw new AttemptAddToFullListException("This list is full");
        }
    }

    /**
     * Remove object from list by index position.
     * @param index - position in list
     * @return - old value from list
     */
    @Override
    public final T remove(final int index) {
        if (checkIndex(index)) {
            T oldValue = elements[index];
            elements[index] = null;
            return oldValue;
        }
        return null;
    }

    /**
     * Clear our list.
     */
    @Override
    public final void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }
        size = 0;
    }

    /**
     * Checks if values are in bounds.
     * @param index - position
     * @return true if values are in bounds
     */
    private static boolean checkIndex(final int index) {
        if (index >= size || index < 0) {
            throw new AttemptAddToFullListException("Incorrect index");
        }
        return true;
    }
}
