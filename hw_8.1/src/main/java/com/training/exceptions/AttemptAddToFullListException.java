package com.training.exceptions;

/**
 * Build exceptions for our EditedList.
 */
public class AttemptAddToFullListException extends RuntimeException {

    /**
     * Constructor of AttemptAddToFullListException
     * which call constructor from Exception.
     * @param message - message which will be displayed in the console
     */
    public AttemptAddToFullListException(final String message) {
        super(message);
    }
}
