package com.training;

import com.training.exceptions.AttemptAddToFullListException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test EditedListTest to cover all methods of this class.
 *
 * @author m_gichevsky
 */
public class EditedListTest {

    private static EditedList<Integer> editedList;

    @Before
    public void init() {
        editedList = new EditedList<Integer>(4);
        editedList.clear();
        editedList.add(5);
    }

    @Test
    public void testIsEmpty() {
        Assert.assertFalse(editedList.isEmpty());
    }

    @Test
    public void testSize() {
        editedList.add(6);
        Assert.assertEquals(2, editedList.size());
        editedList.clear();
    }

    @Test
    public void testSet() {
        editedList.add(1);
        editedList.add(2);
        editedList.add(3);
        Integer testValue = 5;
        editedList.set(1, testValue);
        Assert.assertEquals(testValue, editedList.get(1));
        editedList.clear();
    }

    @Test (expected = AttemptAddToFullListException.class)
    public void testAddToFullList() throws AttemptAddToFullListException {
        editedList.add(1);
        editedList.add(2);
        editedList.add(3);
        editedList.add(4);
        editedList.add(5);
        editedList.clear();
    }

    @Test (expected = AttemptAddToFullListException.class)
    public void testRemoveWrongIndex() throws AttemptAddToFullListException {
        editedList.remove(5);
    }
}
