package com.training;

import java.math.BigDecimal;

/**
 * Atm - provide you using classes CreditCard and DebitCard.
 * @author m_gichevsky
 */
public class Atm {
    private static Card selectedCard;

    /**
     * Constructor - taking selected object.
     * @param card - object of Credit or Debit card
     */
    Atm(final Card card) {
        selectedCard = card;
    }

    /**
     * Get balance from card.
     * @return card balance in BigDecimal value
     */
    public final BigDecimal getBalance() {
        return selectedCard.getBalance();
    }

    /**
     * Will add BigDecimal value to balance.
     * @param addValue - value to be added to balance in BigDecimal
     */
    public final void atmAddToBalance(final BigDecimal addValue) {
        selectedCard.addToBalance(addValue);
    }

    /**
     * Subtract value from card balance.
     * @param withdrawValue - value to be subtracted from balance in BigDecimal
     */
    public final void atmWithdrawFromBalance(final BigDecimal withdrawValue) {
        selectedCard.withdrawFromBalance(withdrawValue);
    }
}
