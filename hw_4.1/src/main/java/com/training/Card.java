package com.training;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Class Card for work with balance.
 * @author m_gichevsky
 */
public class Card {
    public static final int SCALE_VALUE = 2;
    protected String ownerName;
    protected BigDecimal balance;

    /**
     * Constructor - creating new object with parameters.
     * @param ownerName - name of card owner
     * @param value - balance value, input only in BigDecimal
     */
    Card(final String ownerName, final BigDecimal value) {
        this.ownerName = ownerName;
        this.balance = value.setScale(SCALE_VALUE, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Constructor - creating new object with 1 parameter ownerName.
     * @param ownerName - name of card owner
     */
    Card(final String ownerName) {
        this.ownerName = ownerName;
        this.balance = new BigDecimal(0);
    }

    /**
     * This getter gets ownerName value.
     * @return ownerName
     */
    public final String getOwnerName() {
        return ownerName;
    }

    /**
     * This getter gets balance value.
     * @return balance in BigDecimal
     */
    public final BigDecimal getBalance() {
        return balance;
    }

    /**
     * Add to current balance new value.
     * @param addValue value to be added to balance in BigDecimal
     */
    public void addToBalance(final BigDecimal addValue) {
        this.balance = balance.add(addValue).setScale(SCALE_VALUE, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Subtract value from balance.
     * @param withdrawValue value to be subtracted from balance in BigDecimal
     */
    public void withdrawFromBalance(final BigDecimal withdrawValue) {
        this.balance = balance.subtract(withdrawValue).setScale(SCALE_VALUE, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Additional method to display Card balance in different currency (dollars) by
     * providing exchange rate.
     * @param courseOfCurrency - current course of dollar
     * @return value of balance in dollars (BigDecimal)
     */
    public final BigDecimal displayInDollars(final BigDecimal courseOfCurrency) {
        return this.balance.divide(courseOfCurrency, SCALE_VALUE, RoundingMode.HALF_DOWN);
    }
}

