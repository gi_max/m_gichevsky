package com.training;

import java.math.BigDecimal;

/**
 * Class CreditCard - inheritor of class Card who can have negative balance.
 * @author m_gichevsky
 */
public class CreditCard extends Card {

    /**
     * Constructor - creating new object with parameters.
     *
     * @param ownerName - name of card owner
     * @param value     - balance value, input only in BigDecimal
     */
    CreditCard(final String ownerName, final BigDecimal value) {
        super(ownerName, value);
    }

    /**
     * Constructor - creating new object with 1 parameter ownerName.
     *
     * @param ownerName - name of card owner
     */
    CreditCard(final String ownerName) {
        super(ownerName);
    }
}
