package com.training;

import java.math.BigDecimal;

/**
 * Class DebitCard - inheritor of class Card who can't subtract value greater than balance value.
 * @author m_gichevsky
 */
public class DebitCard extends Card{

    /**
     * Constructor - creating new object with parameters.
     *
     * @param ownerName - name of card owner
     * @param value     - balance value, input only in BigDecimal
     */
    DebitCard(final String ownerName, final BigDecimal value) {
        super(ownerName, value);
    }

    /**
     * Constructor - creating new object with 1 parameter ownerName.
     *
     * @param ownerName - name of card owner
     */
    DebitCard(final String ownerName) {
        super(ownerName);
    }

    /**
     * Overriding method which will not allow you to remove the value from the balance
     * is greater than the balance itself.
     *
     * @param withdrawValue value to be subtracted from balance in BigDecimal
     */
    @Override
    public void withdrawFromBalance(final BigDecimal withdrawValue) {
        if (withdrawValue.compareTo(balance) < 0) {
            this.balance = balance.subtract(withdrawValue).setScale(SCALE_VALUE, BigDecimal.ROUND_HALF_UP);
        }
    }

}
