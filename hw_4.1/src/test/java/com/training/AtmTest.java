package com.training;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * AtmTest
 *
 * @author m_gichevsky
 */
public class AtmTest {
    @Test
    public void testAtmAddToBalance() {
        BigDecimal addValue = new BigDecimal("625848.44");
        Card card1 = new DebitCard("First");
        Atm visitor = new Atm(card1);
        visitor.atmAddToBalance(addValue);
        Assert.assertEquals(addValue, visitor.getBalance());
    }

    @Test
    public void testAtmWithdrawFromBalanceDebitCard() {
        BigDecimal testValue = new BigDecimal("500");
        BigDecimal withdraw = new BigDecimal("550");
        Card card1 = new DebitCard("First", testValue);
        Atm visitor = new Atm(card1);
        visitor.atmWithdrawFromBalance(withdraw);
        Assert.assertEquals(testValue.setScale(2, BigDecimal.ROUND_HALF_UP), visitor.getBalance());
    }

    @Test
    public void testAtmWithdrawFromBalanceCreditCard() {
        BigDecimal testValue = new BigDecimal("500");
        BigDecimal withdraw = new BigDecimal("550");
        BigDecimal expected = new BigDecimal("-50");
        Card card1 = new CreditCard("First", testValue);
        Atm visitor = new Atm(card1);
        visitor.atmWithdrawFromBalance(withdraw);
        Assert.assertEquals(expected.setScale(2, BigDecimal.ROUND_HALF_UP), visitor.getBalance());
    }
}
