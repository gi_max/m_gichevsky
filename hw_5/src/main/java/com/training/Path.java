package com.training;

import com.training.components.*;

/**
 * Class Path for realize input command  reading.
 */
public class Path {
    Folder root = new Folder("root/");

    /**
     * Get root for output.
     * @return - root in object of class Folder
     */
    public Folder getRoot() {
        return root;
    }

    /**
     * Method for processing command which starts with root.
     * @param command - string for adding new folders
     */
    public void commandHandler(String command) {
        if (command.trim().startsWith("root")) {
            command = command.substring(command.indexOf('/') + 1);
            root.add(recursion(command));
        }
    }

    /**
     * Recursion which create folders in folders.
     * @param command - string with names of folders
     * @return - returned folder of file for adding to root
     */
    private PathItem recursion(String command) {
        String currentName;
        if (command.indexOf('/') != -1) {
            currentName = command.substring(0, command.indexOf('/') + 1);
            Folder folder = new Folder("\t" + currentName);
            command = "\t" + command.substring(command.indexOf('/') + 1);
            folder.add(recursion(command));
            return folder;
        } else if (command.indexOf('.') != -1) {
            return new File("\t" + command + "\n");
        }
        return null;
    }
}