package com.training;

import org.apache.commons.lang.StringUtils;
import java.util.Scanner;

/**
 * Class Input for input values from command line.
 */
public class Input {

    /**
     * Method for handling user input.
     */
    public static void start() {
        Scanner scanner = new Scanner(System.in);
        String input = StringUtils.EMPTY;
        Path path = new Path();

        System.out.println("Write \" root/.. \" to create new directory");
        System.out.println("Write \" print \" to display created directories");
        System.out.println("Write \" exit \" to exit the program");
        while(!input.equals("exit")) {
            input = scanner.nextLine();
            path.commandHandler(input);
            if (input.equals("print")) {
                System.out.println(path.getRoot().getFileSystemTree());
            }
        }
    }
}