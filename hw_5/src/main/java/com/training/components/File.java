package com.training.components;

/**
 * Class file which store file methods.
 */
public class File implements PathItem {
    private String name;

    /**
     * Get name of file.
     * @return name of file in string
     */
    public String getName() {
        return name;
    }

    /**
     * Constructor will install name value.
     * @param name - file name in string
     */
    public File(String name) {
        this.name = name;
    }

    @Override
    public String getFileSystemTree() {
        return name;
    }
}
