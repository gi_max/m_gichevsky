package com.training.components;

import java.util.LinkedHashSet;

/**
 * Class folder which store methods for folder.
 */
public class Folder implements PathItem {

    private LinkedHashSet<PathItem> children = new LinkedHashSet<>();
    private String name;

    /**
     * Get name of folder.
     * @return name of folder in string
     */
    public String getName() {
        return name;
    }

    /**
     * Constructor will install name value.
     * @param name - name of folder in string
     */
    public Folder(String name) {
        this.name = name;
    }

    @Override
    public String getFileSystemTree() {
        StringBuilder structure = new StringBuilder(name)
                .append(System.getProperty("line.separator"));
        for (PathItem child : children) {
            structure.append(child.getFileSystemTree());
        }
        return structure.toString();
    }

    /**
     * Add folders or files to folder.
     * @param pathItem - folder or file you need to add
     */
    public void add(PathItem pathItem) {
        children.add(pathItem);
    }
}
