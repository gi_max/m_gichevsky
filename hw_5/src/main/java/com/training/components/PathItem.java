package com.training.components;

/**
 * Interface PathItem for create new path.
 */
public interface PathItem {
    /**
     * Will build output tree.
     * @return - structure in string
     */
    String getFileSystemTree();

    /**
     * Will take folder name.
     * @return folder name in string
     */
    String getName();
}
