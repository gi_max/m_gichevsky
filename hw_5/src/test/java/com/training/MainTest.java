package com.training;

import com.training.components.Folder;
import org.junit.Assert;
import org.junit.Test;

/**
 * Folder test.
 *
 * @author m_gichevsky
 */
public class MainTest {

    @Test
    public void testCreatingNewFolder() {
        Folder folder = new Folder("folder");
        Assert.assertEquals("folder", folder.getName());
    }

    @Test
    public void testForAddNewFoldersAndFilesInFolder() {
        Path path = new Path();
        String command = "root/folder1/file.txt";

        path.commandHandler(command);
        System.out.println(path.getRoot().getFileSystemTree());
        Assert.assertEquals("root/", path.getRoot().getName());
    }

    @Test (expected = NullPointerException.class)
    public void testInputDirectoryWithoutFileName() {
        Path path = new Path();
        String command = "root/folder1";
        path.commandHandler(command);
        path.getRoot().getFileSystemTree();
    }
}