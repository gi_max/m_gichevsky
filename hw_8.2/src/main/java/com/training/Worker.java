package com.training;

import jdk.nashorn.internal.objects.annotations.Getter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Worker {
    private String workerName;
    private List<Skill> listOfSkills = new ArrayList<Skill>();

    @Getter
    public List<Skill> getListOfSkills() {
        return listOfSkills;
    }

    public Worker(String workerName, Skill... skill) {
        listOfSkills = new ArrayList<>();
        listOfSkills = Arrays.asList(skill);
    }

    public boolean isApplicable(Skill skill) {

        if (listOfSkills.contains(skill)) {
            return true;
        } else {
            return false;
        }
    }

    public enum Skill {
        ENGINEER,
        ARCHITECT,
        CONTRACTOR,
        ELECTRIC,
        MASON,
        PAINTER;
    }
}
