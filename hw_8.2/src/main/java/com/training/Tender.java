package com.training;

import com.training.exceptions.ModifyClosedTenderException;
import com.training.exceptions.NoApplicableCandidateException;

import java.util.*;

public class Tender {

    private String tenderName;
    private List<Brigade> candidates = new ArrayList<>();
    private Map<Worker.Skill, Integer> requirements = new HashMap<>();
    private Map<String, Integer> result = new HashMap<>();
    private boolean isActive = false;

    public List<Brigade> getCandidates() {
        return candidates;
    }

    public Map<String, Integer> getResult() {
        return result;
    }

    public Map<Worker.Skill, Integer> getNecessaryConditions() {
        return requirements;
    }

    public Tender(String tenderName) {
        isActive = true;
        this.tenderName = tenderName;
    }

    public void addCandidate(Brigade brigade) {
        candidates.add(brigade);
    }

    public void addSkillRequirement(Worker.Skill skill, int numberOfWorkers) {
        requirements.put(skill, numberOfWorkers);
    }

    public void addConditions(Worker.Skill skill, int number) {
        requirements.put(skill, number);
    }

    public String searchMinInMap(Map<String, Integer> result) {
        return Collections.min(result.entrySet(),
                Comparator.comparingInt(Map.Entry::getValue)).getKey();
    }

    public void findApplicable() throws NoApplicableCandidateException {
        if (!isActive) {
            throw new ModifyClosedTenderException("tender is close");
        } else {
            for (Brigade candidate : candidates) {
                if (candidate.isBrigadeApplicable(requirements)) {
                    result.put(candidate.getBrigadeName(), candidate.getBrigadeFinancialPosition());
                }
            }
            if (result.isEmpty()) {
                throw new NoApplicableCandidateException("No applicable candidates");
            }
        }
    }

    public void close() {
        isActive = false;
    }

    public boolean isActive() {
        return isActive;
    }
}
