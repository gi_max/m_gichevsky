package com.training;

import jdk.nashorn.internal.objects.annotations.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Brigade {

    private static List<Worker> listOfWorkers = new ArrayList<Worker>();
    private String brigadeName;
    private int brigadeFinancialPosition;

    @Getter
    public int getBrigadeFinancialPosition() {
        return brigadeFinancialPosition;
    }

    @Getter
    public String getBrigadeName() {
        return brigadeName;
    }

    @Getter
    public static List<Worker> getListOfWorkers() {
        return listOfWorkers;
    }

    public Brigade(String brigadeName, int financialPosition) {
        this.brigadeName = brigadeName;
        this.brigadeFinancialPosition = financialPosition;
    }

    public void addWorker(Worker worker) {
        listOfWorkers.add(worker);
    }

    public boolean isBrigadeApplicable(Map<Worker.Skill, Integer> conditions) {


        for (Map.Entry<Worker.Skill, Integer> entry : conditions.entrySet()) {
            Worker.Skill skill = entry.getKey();
            int numberOfSKills = entry.getValue();
            int number = 0;

            for (Worker worker : listOfWorkers) {
                if (worker.isApplicable(skill)) {
                    number += 1;
                }
            }

            if (number < numberOfSKills) {
                return false;
            }

        }
        return true;
    }

}
