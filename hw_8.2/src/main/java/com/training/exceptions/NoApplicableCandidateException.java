package com.training.exceptions;

public class NoApplicableCandidateException extends RuntimeException{

    public NoApplicableCandidateException(final String errorMessage) {
        super(errorMessage);
    }
}
