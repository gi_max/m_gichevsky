package com.training.exceptions;

public class ModifyClosedTenderException extends RuntimeException {

    public ModifyClosedTenderException (final String errorMessage) {
        super(errorMessage);
    }
}
