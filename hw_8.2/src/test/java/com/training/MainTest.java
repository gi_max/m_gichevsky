package com.training;

import com.training.exceptions.ModifyClosedTenderException;
import com.training.exceptions.NoApplicableCandidateException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Main test class.
 */
public class MainTest {
    private Tender tender;
    private static Brigade technoTeam = new Brigade("Techno Team", 50000);
    private static Brigade buildGroup = new Brigade("Build Group", 40000);
    private static Brigade belBrigade = new Brigade("Bel Brigade",  25000);

    @BeforeClass
    public static void setUpCandidates() {
        technoTeam.addWorker(new Worker("Dima", Worker.Skill.values()));
        technoTeam.addWorker(new Worker("Andrew", Worker.Skill.CONTRACTOR, Worker.Skill.ELECTRIC, Worker.Skill.MASON));
        technoTeam.addWorker(new Worker("Max", Worker.Skill.ARCHITECT, Worker.Skill.ELECTRIC));
        technoTeam.addWorker(new Worker("Pasha", Worker.Skill.PAINTER, Worker.Skill.MASON));
        technoTeam.addWorker(new Worker("George", Worker.Skill.ARCHITECT, Worker.Skill.CONTRACTOR));
        //1 ENGINEER, 3 ARCHITECT, 3 CONTRACTOR, 3 ELECTRIC, 3 MASON, 2 PAINTER

        buildGroup.addWorker(new Worker("Dima", Worker.Skill.values()));
        buildGroup.addWorker(new Worker("Andrew", Worker.Skill.CONTRACTOR, Worker.Skill.ELECTRIC, Worker.Skill.MASON));
        buildGroup.addWorker(new Worker("George", Worker.Skill.ARCHITECT, Worker.Skill.CONTRACTOR));
        buildGroup.addWorker(new Worker("Pasha", Worker.Skill.PAINTER));
        buildGroup.addWorker(new Worker("Vasya", Worker.Skill.ARCHITECT));
        //1 ENGINEER, 3 ARCHITECT, 3 CONTRACTOR, 2 ELECTRIC, 2 MASON, 2 PAINTER

        belBrigade.addWorker(new Worker("Dima", Worker.Skill.values()));
        belBrigade.addWorker(new Worker("Andrew", Worker.Skill.CONTRACTOR, Worker.Skill.ELECTRIC, Worker.Skill.MASON));
        belBrigade.addWorker(new Worker("Pasha", Worker.Skill.PAINTER));
        belBrigade.addWorker(new Worker("Max", Worker.Skill.ARCHITECT, Worker.Skill.ELECTRIC));
        //1 ENGINEER, 2 ARCHITECT, 2 CONTRACTOR, 3 ELECTRIC, 2 MASON, 2 PAINTER
    }

    @Before
    public void setTender() {
        tender = new Tender("National Library");
        tender.addCandidate(technoTeam);
        tender.addCandidate(buildGroup);
        tender.addCandidate(belBrigade);
    }

    @Test
    public void testTender() throws NoApplicableCandidateException {
        tender.addSkillRequirement(Worker.Skill.ENGINEER, 1);
        tender.addSkillRequirement(Worker.Skill.CONTRACTOR, 3);
        tender.addSkillRequirement(Worker.Skill.ELECTRIC, 2);
        tender.addSkillRequirement(Worker.Skill.ARCHITECT, 2);
        tender.addSkillRequirement(Worker.Skill.MASON, 1);
        tender.addSkillRequirement(Worker.Skill.PAINTER, 2);
        tender.findApplicable();
    }

    @Test
    public void testTenderClose() {
        tender.addSkillRequirement(Worker.Skill.ENGINEER, 1);
        tender.addSkillRequirement(Worker.Skill.ELECTRIC, 2);
        tender.addSkillRequirement(Worker.Skill.ARCHITECT, 5);
        try {
            tender.findApplicable();
        } catch (NoApplicableCandidateException exception) {
            tender.close();
        }
        Assert.assertTrue(tender.isActive());
    }

    @Test//(expected = NoApplicableCandidateException.class)
    public void testTenderFails1_ExceptionNoApplicableCandidate() throws NoApplicableCandidateException {
        tender.addSkillRequirement(Worker.Skill.ENGINEER, 1);
        tender.addSkillRequirement(Worker.Skill.ELECTRIC, 2);
        tender.addSkillRequirement(Worker.Skill.ARCHITECT, 5);
        tender.findApplicable();
    }

    @Test//(expected = ModifyClosedTenderException.class)
    public void testTenderFails2_AddSkillRequirement_ExceptionModifyClosedTenderException() {
        tender.close();
        tender.addSkillRequirement(Worker.Skill.ENGINEER, 1);
    }

    @Test//(expected = ModifyClosedTenderException.class)
    public void testTenderFails3_AddCandidate_ExceptionModifyClosedTenderException() {
        tender.close();
        tender.addCandidate(belBrigade);
    }

    @Test(expected = ModifyClosedTenderException.class)
    public void testTenderFails4_FindApplicable_ExceptionModifyClosedTenderException()
            throws NoApplicableCandidateException {
        tender.close();
        tender.findApplicable();
    }
}
