package com.training;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * WordFinderTest.
 *
 * @author m_gichevsky
 */
public class WordFinderTest {

    private WordFinder finder;
    private String text = "Once upon a time a Wolf was lapping at a spring on a hillside, when, looking up, what should he see but a" +
            "Lamb just beginning to drink a little lower down";

    @Before
    public void initialize() {
        finder = new WordFinder(text);
    }

    @Test
    public void testForShowWords() {
        String actual = finder.getList().get(0);
        String expected = "a";
        Assert.assertEquals(expected, actual);
    }

    @Test (expected = NullPointerException.class)
    public void testWordFinderWithNoData() {
        finder = new WordFinder(null);
    }

    @Test
    public void testListEmptyList() {
        text = "";
        finder = new WordFinder(text);
        String actual = finder.getList().get(0);
        Assert.assertEquals("", actual);
    }
}
