package com.training;

import java.util.Scanner;

/**
 * Class Input work with user input.
 */
public class Input {
    /**
     * Scanning user input text and process it.
     */
    public static void start() {
        Scanner scan = new Scanner(System.in);
        String text = scan.nextLine();
        WordFinder finder = new WordFinder(text);
        finder.showWords();
    }
}
