package com.training;

import java.util.*;

/**
 * Class for work with text.
 */
public class WordFinder {
    private String text;
    private List<String> list = new ArrayList<String>();
    private int arraySize;

    /**
     * Getter list.
     * @return - list
     */
    public List<String> getList() {
        return list;
    }

    /**
     * Constructor - set text value and call searchWords method.
     * @param text - text which you need to process
     */
    WordFinder(String text) {
        this.text = text.toLowerCase();
        searchWords();
    }

    /**
     * Find only words in text and sorting them.
     */
    private void searchWords() {
        String[] arr = text.split("\\W+");
        arraySize = arr.length;
        list.addAll(Arrays.asList(arr));
        Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
    }

    /**
     * Find how much the same words, display words and count their coincidences.
     */
    public void showWords() {
        String word;
        int count;
        char firstChar = 0;
        for (int i = 0; i < arraySize; i++) {
            count = 1;
            word = list.get(i);
            if (firstChar != word.charAt(0)) {
                firstChar = word.charAt(0);
                System.out.print(Character.toUpperCase(firstChar) + ": ");
            }
            for (int j = i + 1; j < arraySize; j++) {
                if (word.equals(list.get(j))) {
                    count++;
                }
            }
            System.out.println("\t" + list.get(i) + " - " + count);
            i += count - 1;
        }
    }
}
