package com.training;

/**
 * Main application class.
 *
 * @author m_gichevsky
 */
public class Main {
    /**
     * Application start point.
     * @param args - command line arguments
     */
    public static void main(String[] args) {
        Input.start();
    }
}
